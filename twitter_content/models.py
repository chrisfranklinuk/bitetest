from django.db import models
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _

from mezzanine.twitter import QUERY_TYPE_CHOICES
from mezzanine.pages.models import Page


class TwitterContent(models.Model):

    type = models.CharField(_("Type"), choices=QUERY_TYPE_CHOICES,
                            max_length=10)
    value = models.CharField(_("Value"), max_length=140)

    created_at = models.DateTimeField(
        auto_now_add=True,
        default=now(),
        editable=False,
    )
    updated_at = models.DateTimeField(
        auto_now=True,
        default=now(),
        editable=False,
    )

    class Meta:
        verbose_name = _("Twitter query")
        verbose_name_plural = _("Twitter queries")
        ordering = ("-id",)

    def __unicode__(self):
        return "%s: %s" % (self.get_type_display(), self.value)

    @models.permalink
    def get_absolute_url(self):
        return ('twitter_content_twittercontent_detail', (), {'pk': self.pk})


class TwitterContentGroup(Page):
    name = models.CharField(max_length=20)
    twitter_contents = models.ManyToManyField(TwitterContent)

    class Meta:
        verbose_name = _("Twitter query group")
        verbose_name_plural = _("Twitter query groups")
        ordering = ("-name",)

    def __unicode__(self):
        return self.name
