from django.views.generic import ListView, DetailView, CreateView, DeleteView, UpdateView
from twitter_content.models import TwitterContent
from django.http import HttpResponse
from django.template.response import TemplateResponse

class TwitterContentView(object):
    model = TwitterContent

    def get_template_names(self):
        """Nest templates within twittercontent directory."""
        tpl = super(TwitterContentView, self).get_template_names()[0]
        app = self.model._meta.app_label
        mdl = 'twittercontent'
        self.template_name = tpl.replace(app, '{0}/{1}'.format(app, mdl))
        return [self.template_name]


class TwitterContentBaseListView(TwitterContentView):
    paginate_by = 10


class TwitterContentCreateView(TwitterContentView, CreateView):
    pass


class TwitterContentListView(TwitterContentBaseListView, ListView):
    pass


class TwitterContentDetailView(TwitterContentView, DetailView):
    pass


class TwitterContentDeleteView(TwitterContentView, DeleteView):

    def get_success_url(self):
        from django.core.urlresolvers import reverse
        return reverse('twitter_content_twittercontent_list')


class TwitterContentUpdateView(TwitterContentView, UpdateView):
    pass

