from django.http import HttpResponse
from django.core.management import call_command
import datetime
def getTweets(request):
    call_command('poll_twitter', interactive=False)
    html = "<html><body>Last updated at: %s. Please click <a href='javascript:history.back()'> here </a> to go back. </body></html>" % datetime.datetime.now()
    return HttpResponse(html)
