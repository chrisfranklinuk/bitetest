from django.conf.urls import patterns, url
from twitter_content.views.twittercontent_views import *
from twitter_content.views.views import getTweets

urlpatterns = patterns('',
    url(
        regex=r'^twittercontent/create/$',
        view=TwitterContentCreateView.as_view(),
        name='twitter_content_twittercontent_create'
    ),
    url(
        regex=r'^twittercontent/$',
        view=TwitterContentListView.as_view(),
        name='twitter_content_twittercontent_list'
    ),
    url(
        regex=r'^twittercontent/(?P<pk>\d+?)/$',
        view=TwitterContentDetailView.as_view(),
        name='twitter_content_twittercontent_detail'
    ),
    url(
        regex=r'^twittercontent/(?P<pk>\d+?)/delete/$',
        view=TwitterContentDeleteView.as_view(),
        name='twitter_content_twittercontent_delete'
    ),
    url(
        regex=r'^twittercontent/(?P<pk>\d+?)/update/$',
        view=TwitterContentUpdateView.as_view(),
        name='twitter_content_twittercontent_update'
    ),

    url(r'^get_tweets/$', 'twitter_content.views.views.getTweets'),
)
