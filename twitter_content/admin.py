from django.contrib import admin
from mezzanine.pages.admin import PageAdmin
from .models import TwitterContentGroup, TwitterContent


class TwitterContentInline(admin.TabularInline):
    model = TwitterContentGroup.twitter_contents.through

class TwitterContentGroupAdmin(PageAdmin):
    inlines = (TwitterContentInline,)

admin.site.register(TwitterContent)

admin.site.register(TwitterContentGroup, TwitterContentGroupAdmin)
