# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'TwitterContentGroup'
        db.create_table('twitter_content_twittercontentgroup', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=20)),
        ))
        db.send_create_signal('twitter_content', ['TwitterContentGroup'])

        # Adding M2M table for field twitter_contents on 'TwitterContentGroup'
        db.create_table('twitter_content_twittercontentgroup_twitter_contents', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('twittercontentgroup', models.ForeignKey(orm['twitter_content.twittercontentgroup'], null=False)),
            ('twittercontent', models.ForeignKey(orm['twitter_content.twittercontent'], null=False))
        ))
        db.create_unique('twitter_content_twittercontentgroup_twitter_contents', ['twittercontentgroup_id', 'twittercontent_id'])


    def backwards(self, orm):
        # Deleting model 'TwitterContentGroup'
        db.delete_table('twitter_content_twittercontentgroup')

        # Removing M2M table for field twitter_contents on 'TwitterContentGroup'
        db.delete_table('twitter_content_twittercontentgroup_twitter_contents')


    models = {
        'twitter_content.twittercontent': {
            'Meta': {'ordering': "('-id',)", 'object_name': 'TwitterContent'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2012, 6, 21, 0, 0)', 'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2012, 6, 21, 0, 0)', 'auto_now': 'True', 'blank': 'True'}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '140'})
        },
        'twitter_content.twittercontentgroup': {
            'Meta': {'ordering': "('-name',)", 'object_name': 'TwitterContentGroup'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'twitter_contents': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['twitter_content.TwitterContent']", 'symmetrical': 'False'})
        }
    }

    complete_apps = ['twitter_content']